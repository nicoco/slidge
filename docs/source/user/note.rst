.. note::

  These are the generic user docs for slidge. For
  :term:`Legacy Network`-specific docs, follow these links:
  `discord <https://slidge.im/slidcord/user.html>`_,
  `facebook messenger <https://slidge.im/messlidger/user.html>`_,
  `matrix <https://slidge.im/matridge/user.html>`_,
  `mattermost <https://slidge.im/matteridge/user.html>`_,
  `signal <https://slidge.im/slidgnal/user.html>`_,
  `skype <https://slidge.im/skidge/user.html>`_,
  `steam chat <https://slidge.im/sleamdge/user.html>`_,
  `telegram <https://slidge.im/slidgram/user.html>`_.
  `whatsapp <https://slidge.im/slidge-whatsapp/user.html>`_,
