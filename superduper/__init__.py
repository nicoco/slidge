"""
An example legacy module for slidge.
"""

from . import contact, gateway, group, session

__all__ = "contact", "gateway", "group", "session"
