from slixmpp.plugins.xep_0292 import stanza

from . import vcard4

__all__ = ("stanza", "vcard4")
