Slidge
======

Slidge is a general purpose XMPP gateway framework in python.

Blog posts about slidge for a general overview of what slidge does:

- `0.2.0beta0 <https://www.nicoco.fr/blog/2024/09/01/slidge-02-beta0/>`_
- `0.1.0 <https://nicoco.fr/blog/2024/04/09/slidge-first-release/>`_
- `0.1.0rc1 <https://www.nicoco.fr/blog/2023/01/08/slidge-rc1/>`_
- `0.1.0beta2 <https://www.nicoco.fr/blog/2022/10/30/slidge-beta2/>`_
- `0.1.0beta0 <https://www.nicoco.fr/blog/2022/09/04/slidge-first-beta/>`_

Homepage: `codeberg <https://codeberg.org/slidge>`_

Chat room:
`slidge@conference.nicoco.fr <xmpp:slidge@conference.nicoco.fr?join>`_

Issue tracker: https://codeberg.org/slidge/slidge/issues


.. toctree::

   admin/index
   user/index
   dev/index
   glossary


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
