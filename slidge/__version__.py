from slidge.util.util import get_version  # noqa: F401

# this is modified before publish, but if someone cloned from the repo,
# it can help
__version__ = get_version()
