Finding legacy contacts
=======================

After registration, slidge should add your
:term:`legacy contacts <Legacy Contact>` to your :term:`Roster`.

If you want to message someone that was not automagically added by slidge, you
can sometimes guess their JIDs when the :term:`JID Local Part` is
human-readable, such as a phone number or something like ``name.surname``.

Often times though, the :term:`JID Local Part` of
:term:`legacy contacts <Legacy Contact>`, will be something like a number or a
random word.
Since this usually cannot be guessed, you have to use the "find" :term:`Command`
(or Jabber Search (:xep:`0055`) if your client support it).

If you try to subscribe to a :term:`Legacy Contact`'s presence, this will
generally trigger a "friend/contact request" on the :term:`Legacy network`.
