.. note::

  For legacy module-specific options, refer to their own docs:
  `matridge <https://slidge.im/matridge/config.html>`_,
  `matteridge <https://slidge.im/matteridge/config.html>`_,
  `messlidger <https://slidge.im/messlidger/config.html>`_,
  `skidge <https://slidge.im/skidge/config.html>`_,
  `sleamdge <https://slidge.im/sleamdge/config.html>`_,
  `slidcord <https://slidge.im/slidcord/config.html>`_,
  `slidge-whatsapp <https://slidge.im/slidge-whatsapp/config.html>`_,
  `slidgnal <https://slidge.im/slidgnal/config.html>`_,
  `slidgram <https://slidge.im/slidgram/config.html>`_.
